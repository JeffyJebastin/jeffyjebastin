import paho.mqtt.client as mqtt
import logging
import json
from bin.webhooks import register_webhooks

logger = logging.getLogger(__name__)

with open('/Users/jeffyjebastin/Documents/Precise/bin/manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
client_id = (data['boot'][0]['rest']['credentials']['client_id'])
client_secret = data['boot'][0]['rest']['credentials']['client_secret']
broker_add = (data['boot'][0]['rest']['credentials']['server'])
broker_address = broker_add.split('.', 1)

def on_connect(client, userdata, flags, rc):
    logger.trace("Connected with the code" +str(rc))


def on_subscribe(client, userdata, mid, granted_qos):
    logger.trace("Subscribed")
    logger.info(str(mid))
    logger.info(str(granted_qos))
    logger.info(str(userdata))


def on_publish(client, userdata, mid):
    logger.trace("mid ::" + str(mid))


def on_message(client, userdata, msg):
    logger.trace("message")
    logger.notice(msg.topic)
    logger.notice(str(msg.payload))


def on_disconnect(client, userdata, rc):
    if rc != 0:
        logger.debug("Unexpected Connection")
    else:
        logger.debug("Expected Connection")


client = mqtt.Client()
client.on_connect = on_connect
client.on_subscribe = on_subscribe
client.on_message = on_message
client.on_disconnect = on_disconnect

def mqtt_config(access_token):
    client.username_pw_set(client_id, access_token)
    logger.debug("Before" + str(client._ssl))
    if not client._ssl:
        client.tls_set()
    client.enable_logger()
    client.connect(broker_address[1], 8889, 5)
    client.loop_start()
    client.subscribe("/v3/managers/" + client_id + "/channels/#", qos=1)
    return register_webhooks(access_token)


def mqtt_deconfig():
    logger.debug("Unsubscribe" +str(client._ssl))
    client.Unsubscribe("/v3/managers/" + client_id + "/channels/#")
    client.loop_stop()
    client.disconnect()
    client.disable_logger()