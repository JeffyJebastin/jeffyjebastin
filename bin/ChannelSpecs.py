import requests
import json


#config file
with open('manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
client_id=(data['boot'][0]['rest']['credentials']['client_id'])
client_secret=(data['boot'][0]['rest']['credentials']['client_secret'])
broker_add=(data['boot'][0]['rest']['credentials']['server'])

#access_token
headers = {
       'Content-Type': 'application/json',
    }

data = {
       'client_id': client_id,
       'client_secret': client_secret,
       'response_type':'client_credentials',
       'scope':'manager'
    }

response = requests.get(broker_add + "/v3/auth/authorize", headers=headers, params=data)
data = response.json()
access_token = data['access_token']
headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+access_token+''
    }


def main_fn():
    #Channel_Specs
    values_ChannelSpecs_MainFn = {
    "icon": "https://api.something.something/else",
    "image": "https://api.something.something/else",
    "multiple": True,
    "name": "NAME",
    "namespace": "NAMESPACE",
    "state": "active"
    }
   
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/channel-specs', headers=headers, data=json.dumps(values_ChannelSpecs_MainFn))
    response_body = request.json()
    p_id=response_body['id']
    print(p_id)
    Component_Specs(p_id)
    #Channel Template
    values_ChannelTemplates = {
       "active_ts": "2017-09-06T15:48:50.933+0100",
       "brand": "BRAND",
       "channelspec_id": p_id,
       "description": "DESCRIPTION",
       "icon": "https://api.something.something/else",
       "image": "https://api.something.something/else",
       "inactive_ts": "2017-09-06T15:48:50.933+0100",
       "manufacturer": "MANUFACTURER",
       "name": "NAME",
       "required_capability": "REQUIRED_CAPABILITY",
       "state": "active"

    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/channel-templates', headers=headers, data=json.dumps(values_ChannelTemplates))
    response_body = request.json()
    print("Channel Template"+str(response_body))
    channeltemplate_id = response_body['id']
    values_AssocManager = {
                 "cdata":{},
                 "channeltemplate_id":channeltemplate_id,
                 "order_index":1
    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/managers/'+client_id+'/channel-templates', headers=headers, data=json.dumps(values_AssocManager))
    response_body = request.json()
    print("Associate Manager"+str(response_body))

    


def Component_Specs(p_id):
    #Component 1
    values_ComponentSpecs = {
        "parent" :p_id,
        "classes": ["com.muzzley.components.component_a"],
        "icon":"https://api.something.something/else",
        "multiple":True,
        "name":"ComponentA",
        "namespace":"component_a",
        "state":"active"
    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/component-specs', headers=headers, data=json.dumps(values_ComponentSpecs))
    response_body = request.json()
    print("Component 1 "+str(response_body))
    p_id_comp1=response_body['id']
    Property_Specs_1(p_id_comp1)
    #Component 2
    values_ComponentSpecs2 = {
        "parent" : p_id,
        "classes":["com.muzzley.components.component_b"],
        "icon":"https://api.something.something/else",
        "multiple":True,
        "name":"ComponentB",
        "namespace":"component_b",
        "state":"active"
    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/component-specs', headers=headers, data=json.dumps(values_ComponentSpecs2))
    response_body = request.json()
    print("Component 2 "+str(response_body))
    p_id_comp2=response_body['id']
    Property_Specs_1(p_id_comp2)



def Property_Specs_1(p_id_comp):
    #Component Property 1
    values_PropertySpecs1 = {
        "parent" : p_id_comp,
        "classes":["com.muzzley.properties.access"],
        "icon":"",
        "metric_system_units":{ },
        "multiple":True,
        "name":"Access",
        "namespace":"access",
        "schema_id":"v1_connection-status",
        "state":"active",
        "unit_id":"none"
    }   
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/property-specs', headers=headers, data=json.dumps(values_PropertySpecs1))
    response_body = request.json()
    print(" Property 1"+str(response_body))
    p_id_comp1_prop1=response_body['id']
    #Component Property 2
    values_PropertySpecs2 ={
        "parent" : p_id_comp,
        "classes":["com.muzzley.properties.property_a"],
        "icon":"",
        "metric_system_units":{ },
        "multiple":True,
        "name":"PropertyA",
        "namespace":"property_a",
        "schema_id":"v1_number",
        "state":"active",
        "unit_id":"none"
    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/property-specs', headers=headers, data=json.dumps(values_PropertySpecs2))
    response_body = request.json()
    print(" Property 2"+str(response_body))
    p_id_comp1_prop2=response_body['id']
    #Component Property 3
    values_PropertySpecs3 = {
        "parent" : p_id_comp,
        "classes":["com.muzzley.properties.property_b"],
        "icon":"",
        "metric_system_units":{ },
        "multiple":True,
        "name":"PropertyB",
        "namespace":"property_b",
        "schema_id":"v1_string",
        "state":"active",
        "unit_id":"none"
    }
    request = requests.post('https://api.platform.integrations.muzzley.com/v3/property-specs', headers=headers, data=json.dumps(values_PropertySpecs3))
    response_body = request.json()
    print(" Property 3"+str(response_body))
    p_id_comp1_prop3=response_body['id']

main_fn()