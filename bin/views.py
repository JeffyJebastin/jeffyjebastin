
from flask import Flask, render_template, redirect, Response
from flask import request
import requests
import json
import threading
import logging
import configparser
from datetime import datetime
from bin.mqtt import mqtt_config
from bin import app
from bin.webhooks import authorize, receive_token_webhook, list_devices, select_devices

logger = logging.getLogger(__name__)

config = configparser.RawConfigParser()
confirmation_hash = 0

with open('/Users/jeffyjebastin/Documents/Precise/bin/manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
client_id = (data['boot'][0]['rest']['credentials']['client_id'])
client_secret = data['boot'][0]['rest']['credentials']['client_secret']
broker_add = (data['boot'][0]['rest']['credentials']['server'])
broker_address = broker_add.split('.', 1)


def signin():
    headers = {
        'Content-Type': 'application/json',
    }

    data = {
        'client_id': client_id,
        'client_secret': client_secret,
        'response_type': 'client_credentials',
        'scope': 'manager',
        'state':'active'
    }
    logger.trace("inside the function :: signin()")
    response = requests.get(broker_add + "/v3/auth/authorize", headers=headers, params=data)
    data = response.json()
    print('Data'+str(data))
    logger.debug(data)
    host = data['endpoints']['http']
    access_token = data['access_token']
    refresh_token = data['refresh_token']
    logger.debug("HOST  ::  " + host)
    logger.debug("ACCESS_TOKEN  ::  " + access_token)
    logger.debug("REFRESH_TOKEN  ::  " + refresh_token)
    logger.trace("End of signin()")
    return mqtt_config(access_token)

@app.route('/authorize/', methods=['GET', 'POST'])
def dummy_authorize():
    print('Entering')
    ch =request.headers.get('Authorization')
    print(ch)
    confirm = ch.split(' ', 1)[1]
    print(confirm)
    return authorize(confirm)
    

    
@app.route('/receive_token/', methods=['GET', 'POST'])
def dummy_receive_token():
    ch = request.headers.get('Authorization')
    confirm = ch.split(' ', 1)[1]
    return receive_token_webhook(confirm)
    


@app.route('/list_devices/', methods=['GET', 'POST'])
def dummy_list_devices():
    ch = request.headers.get('Authorization')
    confirm = ch.split(' ', 1)[1]
    return list_devices(confirm)
    


@app.route('/select_devices/', methods=['GET', 'POST']) 
def dummy_select_devices():
    ch = request.headers.get('Authorization')
    confirm = ch.split(' ', 1)[1]
    return select_devices(confirm)   






