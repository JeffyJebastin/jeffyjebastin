import redis
from flask import Flask, request, Response
import requests
import json
from flask import jsonify
import logging


logger = logging.getLogger(__name__)

headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer adzpl60yojpqv1qgxhlyqoibmfcx6koszfl3b67pxu53a2hwozusynhnvyvk2oojg397c9902t03s0ogrhhi84q4ge1gu5q5840r5ixd48rpkmq55fxsv1d5embvvcyh'
           }

with open('/Users/jeffyjebastin/Documents/Precise/bin/manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
client_id=(data['boot'][0]['rest']['credentials']['client_id'])

def redis_fn(request):
    r = redis.StrictRedis(host="localhost", port=6379, db=0)
    hash_n = 'managers/Persistence/'
    hash_name = str(hash_n) + str(client_id)
    r.hset(hash_name,
           "/".join([
               request.headers['X-Client-Id'],
               request.headers['X-Owner-Id'],
               request.headers['X-Channeltemplate-Id']
           ]),
           request.get_json()
           )
    logger.debug(request.get_json())

def list_devices_details():
    devices = [
        {
            "content": "kitchen frige",
            "id": "9323u2450nwetu"
        },
        {
            "content": "microwave",
            "id": "qiwjfoiasqp34i"
        }
    ]
    return devices


def select_devices_details():
    logger.debug("Inside the function :: select_devices()")
    r = redis.StrictRedis(host="localhost", port=6379, db=0)
    logger.debug(str(r))
    hash_n = 'managers/Persistence/'
    hash_name = str(hash_n) + str(client_id)

    _id = request.get_json()['channels']

    # for it in list_id:
    #     print(it)
    channel = []
    for it in _id:

        check = int(r.hexists(hash_name, it["id"]))

        if check == 1:
            val = r.hget(hash_name, it["id"])
            node = {
                'id': val
            }
            channel.append(node)
        else:
            print("else")
            name = 'hello'
            response = requests.post('https://api.platform.integrations.muzzley.com/v3/managers/self/channels',
                                     headers=headers,
                                     data=json.dumps({'name': name,
                                                      'channeltemplate_id': request.headers['X-Channeltemplate-Id']
                                                      }))
            logger.debug(response.text)
            print(it["id"])
            print(response.json())
            r.hset(hash_name, it["id"], response.json()["id"])
            channel.append(response.text)
        _next(str(it))
    return Response(
        response=json.dumps(str(channel)),
        mimetype='applications/json',
        status=200)


def _next(it):
    response = requests.post('https://api.platform.integrations.muzzley.com/v3/channels/' + str(it) + '/grant-access',
                             headers=headers, data=json.dumps({'client_id': request.headers['X-Client-Id'], 'role': 'application'}))

    response = requests.post('https://api.platform.integrations.muzzley.com/v3/channels/' + str(it) + '/grant-access', headers=headers,
                             data=json.dumps({'client_id': request.headers['X-Owner-Id'], 'requesting_client_id': request.headers['X-Client-Id'], 'role': 'user'}))