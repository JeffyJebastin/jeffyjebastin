from flask import Flask, render_template, redirect
from flask import request as r
import requests
import json



CLIENT_ID = '59bfbc1e3164809da78b66eb'
CLIENT_SECRET = 'eBKMlXQxdMRqGOlNznfi2FqCr9X84C2wdg'

app = Flask(__name__)


@app.route('/')
def sign():
    return "<form action='/signin' method='get'><button type='submit'>Sign in</button></form>"

# Authorization Code type authentication flow


@app.route('/signin/', methods=['GET'])
def signin():
    # Test if "code" is provided in get parameters (that would mean that user has already accepted the app and has been redirected here)
    if r.args.get('code'):
        code = r.args.get('code')
        print(code)
        payload = {'grant_type': 'authorization_code',
                   'client_id': CLIENT_ID,
                   'client_secret': CLIENT_SECRET,
                   'code': code,
                   'redirect_uri': 'http://localhost:5000/signin'}
        try:
                response = requests.post("https://api.netatmo.com/oauth2/token", data=payload)
                response.raise_for_status()
                access_token = response.json()["access_token"]
                refresh_token = response.json()["refresh_token"]
                scope = response.json()["scope"]
                print(access_token)
                print("<p>Your access_token is:" + access_token + ", scope = " + str(scope) + "</p>")
                #return redirect("http://localhost:5000/third/")
                return list_devices(access_token)

        except requests.exceptions.HTTPError as error:
                print(error.response.status_code, error.response.text)
    # Test if "error" is provided in get parameters (that would mean that the user has refused the app)
    elif r.args.get('error') == 'access_denied':
        return "The user refused to give access to his Netatmo data"
    # If "error" and "code" are not provided in get parameters: the user should be prompted to authorize your app
    else:

        payload = {'client_id': CLIENT_ID,
                   'redirect_uri': "http://localhost:5000/signin",
                   # station read_thermostat write_thermostat
                   'scope': 'read_station read_camera read_thermostat write_thermostat read_homecoach',
                   'state': 'login'}
        try:
                response = requests.post("https://api.netatmo.com/oauth2/authorize", params=payload)
                print(response)
                response.raise_for_status()
                return redirect(response.url, code=302)
        except requests.exceptions.HTTPError as error:
                print(error.response.status_code, error.response.text)
                


def list_devices(access_token):
    names_dic = {}

    params = {'access_token' : access_token, 
            }

    try:
                response = requests.post("https://api.netatmo.com/api/getstationsdata", params=params)
                print(response.raise_for_status())
                data = response.json()["body"]
                print(data)
                a = json.dumps(data)  # converting json to string
                b = json.loads(a)  # converting string to dictionary
                print("b value" +str(b))
                for item in b["devices"]:
                    print("item=" + str(item))
                    name = item["station_name"]
                    print("station_name = " + name)
                    names_dic['item']=name
                print("ZZZZ" +str(names_dic)) 
                y = b.items() # converting to list 
                print("Y value is" +str(y)) 

    except requests.exceptions.HTTPError as error:
            print(error.response.status_code, error.response.text)
            return None

 #   elif device_id == '1':
    try:
                response = requests.post("https://api.netatmo.com/api/gethomedata", params=params)
                print(response.raise_for_status())
                data = response.json()["body"]
                print(data)
                c = json.dumps(data)  # json to string
                d = json.loads(c)  # string to dict
                for it in d["homes"]:
                    print("products" + str(it))
                    name2 = it["name"]
                    print("Name = " + name2)
                    names_dic['it']=name2
                z = d.items() 
                print("Z value is"+str(z))

                #g = "Name = " + name2 + "----------------------------------------------->"
                homes = data["homes"]
                #return g, data
                #return names_list
                print("AAAA" +str(names_dic))
                return show_devices(names_dic,a,c,params,access_token)
                

    except requests.exceptions.HTTPError as error:
            print(error.response.status_code, error.response.text)
            return None
    
    

#@app.route('/third/')
def show_devices(names_dic,y,z,params,access_token):
    print("show_devices :: WS"+y)
    print("show_devices :: cam"+z)
    print("show_devices :: params"+str(params))
    return render_template('Device.html', devices=names_dic, ws=y, cam=z, params=params,access_token=access_token)


@app.route('/result', methods=['POST', 'GET'])
def output():
    
    if r.method == 'POST':
        data = r.form
        print("data = " +str(data))
        id = data.get('device')  # gets the device id
        print("BBBBB"+str(id))
        info = signin()
        key=r.form.get("device")
        #params=r.form.get("params")
        #print("result->output :: params"+params)
        access_token=r.form.get("access_token")
        params = {'access_token' : access_token, 
            }
        if(key=='item'):
            
                response = requests.post("https://api.netatmo.com/api/getstationsdata", params= params)
                print(response.raise_for_status())
                data = response.json()["body"]
                print(data)
        else:
                response = requests.post("https://api.netatmo.com/api/gethomedata", params=params)
                print(response.raise_for_status())
                data = response.json()["body"]
                print(data)   
            
        print("output :: KEY"+str(key))
        select =(data.get("ws"))
        select1 =(r.form.get("cam"))
        print('output :: DATUM'+str(select))
        device = select
        # print id
        print(str(select))
        #print(device1)
        return render_template("output.html", device=data, key=key)
    return render_template("student.html")


if __name__ == "__main__":
    app.run(debug=True)