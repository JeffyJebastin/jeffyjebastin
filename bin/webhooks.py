from flask import Flask, request, Response
import logging
import json, requests
from bin.aux_webhooks_fn import list_devices_details, redis_fn, select_devices_details

app = Flask(__name__)
logger = logging.getLogger(__name__)

confirmation_hash = 0
with open('/Users/jeffyjebastin/Documents/Precise/bin/manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
client_id = (data['boot'][0]['rest']['credentials']['client_id'])
client_secret = data['boot'][0]['rest']['credentials']['client_secret']
broker_add = (data['boot'][0]['rest']['credentials']['server'])
broker_address = broker_add.split('.', 1)

def register_webhooks(access_token):
    headers_Webhooks = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+access_token+''
    }
    values = """
      {
        "authorize": "https://fake-test.integrations.muzzley.com/authorize",
        "receive_token": "https://fake-test.integrations.muzzley.com/receive_token",
        "devices_list": "https://fake-test.integrations.muzzley.com/device_list",
        "select_device": "https://fake-test.integrations.muzzley.com/select_device"
       }
    """

    request = requests.patch(broker_add + '/v3/managers/' +
                             client_id + '', data=values, headers=headers_Webhooks)

    response_body = request.json()
    print(str(response_body))
    global confirmation_hash
    confirmation_hash = response_body['confirmation_hash']
    logger.alert(confirmation_hash)
    logger.alert(response_body)

#Authorize WebHook
def authorize(confirm):
    print('Entered function')
    logger.debug("\n\n\n\n**************************AUTHORIZE******************************")
    logger.notice(request.data)
    if(confirmation_hash == confirm):
        logger.debug("confirmed :: " + str(confirm))
        response = { "location": 
            [
                {
                    "method": "get",
                    "url": "https://auth.netatmo.com/access/checklogin?app_id={client_id}&app_secret={client_secret}",
                    "headers": {
                        "X-Sender": "{client_id}"
                    }
                },
                {
                    "method": "get",
                    "url": "https://dev.netatmo.com/myaccount/",
                    "headers": {
                        "Authorization": "code {code}"
                    }
                }
            ]
        }
        print(str(response))
        return Response(
            response=json.dumps(response),
            status=200,
            mimetype='application/json'
        )

    else:
        return "No values available"


#Receive Token Webhooks
def receive_token_webhook(confirm):
    logger.debug(
        "\n\n\n\n**************************RECEIVE_TOKEN******************************")
    if(confirmation_hash == confirm):
        logger.debug("confirmed :: " + str(confirm))
        redis_fn(request)
        return Response(
            status=200
        )

    else:
        return None


#List Devices Webhook
def list_devices(confirm):
    logger.debug(
        "\n\n\n\n**************************LIST_DEVICES******************************")
    if(confirmation_hash == confirm):
        logger.debug("confirmed :: " + str(confirm))
        data = list_devices_details()
        return Response(
            response=json.dumps(data),
            status=200,
            mimetype='application/json'
        )

    else:
        return None


#Select Devices Webhook
def select_devices(confirm):
    logger.debug(
        "\n\n\n\n**************************SELECT_DEVICES******************************")
    if(confirmation_hash == confirm):
        logger.debug("confirmed :: " + str(confirm))
        return select_devices_details()
