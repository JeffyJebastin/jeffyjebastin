import logging
from flask import Flask
import json
from datetime import datetime


app = Flask(__name__)

EMERGENCY = 10
logging.addLevelName(EMERGENCY, 'EMERGENCY')
ALERT = 9
logging.addLevelName(ALERT, 'ALERT')
CRITICAL = 8
logging.addLevelName(CRITICAL, 'CRITICAL')
ERROR = 7
logging.addLevelName(ERROR, 'ERROR')
WARNING = 6
logging.addLevelName(WARNING, 'WARNING')
NOTICE = 5
logging.addLevelName(NOTICE, 'NOTICE')
INFO = 4
logging.addLevelName(INFO, 'INFO')
DEBUG = 3
logging.addLevelName(DEBUG, 'DEBUG')
TRACE = 2
logging.addLevelName(TRACE, 'TRACE')
VERBOSE = 1 
logging.addLevelName(VERBOSE, 'VERBOSE')


def emergency(self, message, *args, **kws):
    self.log(EMERGENCY, message, *args, **kws) 
logging.Logger.emergency = emergency

def alert(self, message, *args, **kws):
    self.log(ALERT, message, *args, **kws) 
logging.Logger.alert = alert

def critical(self, message, *args, **kws):
    self.log(CRITICAL, message, *args, **kws) 
logging.Logger.critical = critical

def error(self, message, *args, **kws):
    self.log(ERROR, message, *args, **kws) 
logging.Logger.error = error

def warning(self, message, *args, **kws):
    self.log(WARNING, message, *args, **kws) 
logging.Logger.warning = warning

def notice(self, message, *args, **kws):
    self.log(NOTICE, message, *args, **kws) 
logging.Logger.notice = notice

def info(self, message, *args, **kws):
    self.log(INFO, message, *args, **kws) 
logging.Logger.info = info

def debug(self, message, *args, **kws):
    self.log(DEBUG, message, *args, **kws) 
logging.Logger.debug = debug

def trace(self, message, *args, **kws):
    self.log(TRACE, message, *args, **kws) 
logging.Logger.trace = trace

def verbose(self, message, *args, **kws):
    self.log(VERBOSE, message, *args, **kws) 
logging.Logger.verbose = verbose


logger = logging.getLogger(__name__)
with open('/Users/jeffyjebastin/Documents/Precise/bin/manager-fake-test-contained.conf') as json_data_file:
    data = json.load(json_data_file)
level = data['$log']['level']
try:
    f = data['$log']['file']

except KeyError:
    f = '/var/log/syslog.log'   


#Create a File Handler
file_handler = logging.FileHandler(f)
logger.setLevel(10-level)

#Create a logging format
formatter=logging.Formatter('%(asctime)s %(name)s : %(levelname)s | '+str(datetime.now())+' | %(message)s | %(processName)s : %(process)d | %(filename)s | %(lineno)s','%b %d  %H:%M:%S')
file_handler.setFormatter(formatter)

#add the handlers to the logger
logger.addHandler(file_handler)

# logger.emergency('Emergency')
# logger.alert('Alert')
# logger.critical('Critical')
# logger.error('Error')
# logger.warning('Warning')
# logger.notice('Notice')
# logger.info('Info')
# logger.debug('Debug')
# logger.trace('Trace')